window.onload = function() {
  chrome.storage.sync.get('domain_hound_extratorrent', function(data) {

    var lastTimeString = data.domain_hound_extratorrent;
    var curTimeDate = new Date();
    var lastTimeDate = Date.parse(lastTimeString);
    var diff = curTimeDate-lastTimeDate;

    document.getElementById("status").innerText = "You last visited https://extratorrent.cc/ on \n" + lastTimeString + "\n" + timeConversion(diff) + " ago";
  });
}

function timeConversion(millisec) {

        var seconds = (millisec / 1000).toFixed(1);

        var minutes = (millisec / (1000 * 60)).toFixed(1);

        var hours = (millisec / (1000 * 60 * 60)).toFixed(1);

        var days = (millisec / (1000 * 60 * 60 * 24)).toFixed(1);

        if (seconds < 60) {
            return seconds + " seconds";
        } else if (minutes < 60) {
            return minutes + " minutes";
        } else if (hours < 24) {
            return hours + " hours";
        } else {
            return days + " days"
        }
    }
